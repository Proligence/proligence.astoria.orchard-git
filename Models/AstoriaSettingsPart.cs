using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using Orchard.ContentManagement;
using Orchard.Environment.Extensions;
using Proligence.Astoria.Orchard.Settings;

namespace Proligence.Astoria.Orchard.Models
{
    public class AstoriaSettingsPart : ContentPart<AstoriaSettingsPartRecord>, IAstoriaSettings
    {
        public IList<RuntimeSettings> RuntimeSettings { get; set; }
        
        public IList<ServiceSettings> ServiceSettings
        {
            get
            {
                var json = new JsonSerializer();
                var obj = json.Deserialize(new StringReader(this.Record.ServiceSettings ?? ""), typeof(IEnumerable<ServiceSettings>));
                var settings = (obj as IList<ServiceSettings> ?? new List<ServiceSettings>()).ToDictionary(s => s.TypeName);
                var settingsFromPart = ServiceSettingsFromPartSettings;

                var keys = settings.Keys.Union(settingsFromPart.Keys);

                return keys
                    // fetching both current and part-provided settings
                    .Select(key => new Tuple<ServiceSettings, ServiceSettings>(
                                       settings.ContainsKey(key) ? settings[key] : null,
                                       settingsFromPart.ContainsKey(key) ? settingsFromPart[key] : null))
                    // filtering if both are nulls
                    .Where(t => (t.Item1 ?? t.Item2) != null)
                    .Select(t => {
                                // item1 has priority - if it's empty then we'll take the one from part settings, if any
                                if (t.Item1 != null && t.Item2 != null) {
                                    return !string.IsNullOrWhiteSpace(t.Item1.Url) ? t.Item1 : t.Item2;
                                }

                                return t.Item1 ?? t.Item2;
                            })
                    .ToList();
            }
            set
            {
                var json = new JsonSerializer();
                var writer = new StringWriter();
                json.TypeNameHandling = TypeNameHandling.None;
                json.Serialize(writer, value);
                this.Record.ServiceSettings = writer.ToString();
            }
        }

        /// <summary>
        /// Retrieves settings specified in part settings (eg. coming from recipe), if any.
        /// </summary>
        protected IDictionary<string, ServiceSettings> ServiceSettingsFromPartSettings
        {
            get
            {
                return Settings
                    .Where(kv => kv.Key.StartsWith("ServiceSettings.") && !string.IsNullOrWhiteSpace(kv.Value))
                    .Select(kv =>
                        new ServiceSettings
                        {
                            TypeName = kv.Key.Substring("ServiceSettings.".Length).Trim(),
                            Url = kv.Value.Trim()
                        })
                    .ToDictionary(set => set.TypeName);
            }
        }
    }
}