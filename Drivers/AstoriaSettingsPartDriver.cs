using Autofac;
using Orchard;
using Orchard.ContentManagement;
using Orchard.ContentManagement.Drivers;
using Orchard.Environment.Extensions;
using Orchard.Localization;
using Orchard.UI.Notify;
using Proligence.Astoria.Orchard.Core;
using Proligence.Astoria.Orchard.Models;
using Proligence.Astoria.Orchard.Services;
using Proligence.Astoria.Orchard.Settings;

namespace Proligence.Astoria.Orchard.Drivers 
{
    /// <summary>
    /// Content part driver for displaying/editing a Collection part
    /// </summary>
    public class AstoriaSettingsPartDriver : ContentPartDriver<AstoriaSettingsPart>
    {
        private readonly IOrchardServices _services;
        private readonly IAstoriaSettingsService _settings;

        private const string TemplateName = "Parts/Astoria.Settings";

        public AstoriaSettingsPartDriver(IOrchardServices services, IAstoriaSettingsService settings) 
        {
            _services = services;
            _settings = settings;

            T = NullLocalizer.Instance;
        }

        public Localizer T { get; set; }

        protected override DriverResult Editor(AstoriaSettingsPart part, dynamic shapeHelper)
        {
            var editor = shapeHelper.EditorTemplate(TemplateName: TemplateName, Model: part, Prefix: Prefix);
            return ContentShape("Parts_Astoria_Settings", () => editor).OnGroup("astoria");
        }

        protected override DriverResult Editor(AstoriaSettingsPart part, IUpdateModel updater, dynamic shapeHelper)
        {
            if (!updater.TryUpdateModel(part, Prefix, null, null))
            {
                _services.Notifier.Error(T("Could not save Astoria services' settings!"));
                _services.TransactionManager.Cancel();
            }
            else
            {
                // Fires change notification for all interceptors.
                // TODO: Narrow the interceptors notified to those with changed settings
                foreach (IRuntimeSettings setting in _settings.AllRuntime())
                {
                    var scope = _services.WorkContext.Resolve<ILifetimeScope>();
                    var interceptor = scope.Resolve(
                        typeof(IAstoriaServiceCallInterceptor<>).MakeGenericType(setting.Type)) as IAstoriaInterceptor;

                    if (interceptor != null)
                    {
                        interceptor.Notify();
                    }
                }

                _services.Notifier.Information(T("Astoria services' settings saved successfully."));
            }

            return Editor(part, shapeHelper);
        }
    }
}