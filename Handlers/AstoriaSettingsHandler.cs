using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel;
using Orchard.ContentManagement;
using Orchard.ContentManagement.Handlers;
using Orchard.Data;
using Orchard.Localization;
using Proligence.Astoria.Client;
using Proligence.Astoria.Orchard.Models;
using Proligence.Astoria.Orchard.Settings;

namespace Proligence.Astoria.Orchard.Handlers
{
    public class AstoriaSettingsHandler : ContentHandler
    {
        public AstoriaSettingsHandler(
            IRepository<AstoriaSettingsPartRecord> repository,
            IEnumerable<IAstoriaService> services)
        {
            T = NullLocalizer.Instance;
            Filters.Add(StorageFilter.For(repository));
            Filters.Add(new ActivatingFilter<AstoriaSettingsPart>("Site"));

            OnLoaded<AstoriaSettingsPart>((ctx, part) =>
            {
                var newSettings = services.Select(
                    s =>
                    {
                        Type type = s.GetType().GetInterfaces().FirstOrDefault(
                            t => typeof(IAstoriaService).IsAssignableFrom(t) && t != typeof(IAstoriaService));

                        if (type == null)
                        {
                            throw new InvalidOperationException(
                                "Failed to find Astoria service interface for '" + s.GetType().FullName + "'.");
                        }

                        var description = (DescriptionAttribute)Attribute.GetCustomAttribute(type, typeof(DescriptionAttribute));

                        return new RuntimeSettings
                        {
                            Type = type,
                            TypeName = type.Name,
                            Instance = s,
                            Description = description != null ? description.Description : "No description provided",
                            Operations = type.GetMethods().Where(
                                m => Attribute.IsDefined(m, typeof(OperationContractAttribute)))
                        };
                    });
                    
                part.RuntimeSettings = newSettings.ToList();
            });
        }

        public Localizer T { get; set; }

        protected override void GetItemMetadata(GetContentItemMetadataContext context)
        {
            if (context.ContentItem.ContentType != "Site")
            {
                return;
            }

            base.GetItemMetadata(context);
            context.Metadata.EditorGroupInfo.Add(new GroupInfo(T("Astoria")));
        }
    }
}