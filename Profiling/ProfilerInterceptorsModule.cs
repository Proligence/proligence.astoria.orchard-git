﻿using Autofac;
using Orchard.Environment.Extensions;
using Proligence.Astoria.Orchard.Core;

namespace Proligence.Astoria.Orchard.Profiling
{
    /// <summary>
    /// Configures dependency injection container for Astoria client library.
    /// </summary>
    [OrchardFeature("Proligence.Astoria.Profiling")]
    public class ProfilerInterceptorsModule : Module
    {
        /// <summary>
        /// Override to add registrations to the container.
        /// </summary>
        /// <param name="builder">The builder through which components can be registered.</param>
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterGeneric(typeof(ProfiledAstoriaServiceCallInterceptor<>)).As(
                typeof(IAstoriaServiceCallInterceptor<>)).InstancePerMatchingLifetimeScope("shell");
        }
    }
}