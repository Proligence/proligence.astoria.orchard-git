﻿using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Routing;
using Orchard.Mvc.Routes;

namespace Proligence.Astoria.Orchard
{
    public class Routes : IRouteProvider
    {
        public void GetRoutes(ICollection<RouteDescriptor> routes)
        {
            foreach (RouteDescriptor routeDescriptor in GetRoutes())
            {
                routes.Add(routeDescriptor);
            }
        }

        public IEnumerable<RouteDescriptor> GetRoutes()
        {
            return new[] {
                new RouteDescriptor {
                    Priority = 25,
                    Route = new Route(
                        "Admin/Astoria/{action}",
                        new RouteValueDictionary {
                            {"area", "Proligence.Astoria.Orchard"},
                            {"controller", "Admin"},
                            {"action", "Index"},
                        },
                        new RouteValueDictionary(),
                        new RouteValueDictionary {
                            {"area", "Proligence.Astoria.Orchard"}
                        },
                        new MvcRouteHandler())
                }
            };
        }
    }
}