﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Castle.Core.Logging;
using Orchard;
using Orchard.Localization;
using Orchard.UI.Admin;
using Orchard.UI.Notify;
using Proligence.Astoria.Client;
using Proligence.Astoria.Client.Discovery;

namespace Proligence.Astoria.Orchard.Controllers
{
    [Admin, ValidateInput(false)]
    public class AdminController : Controller
    {
        private readonly IEnumerable<IAstoriaService> _allServices;
        private readonly IOrchardServices _services;
        private readonly IServiceDiscoverer _discoverer;

        public AdminController(
            IEnumerable<IAstoriaService> allServices,
            IOrchardServices services,
            IServiceDiscoverer discoverer)
        {
            _allServices = allServices;
            _services = services;
            _discoverer = discoverer;

            T = NullLocalizer.Instance;
            Logger = NullLogger.Instance;
        }

        public Localizer T { get; set; }

        public ILogger Logger { get; set; }

        /// <summary>
        /// Runs the discovery of Astoria-based services in local subnet.
        /// </summary>
        [HttpGet]
        public ActionResult Discover()
        {
            try
            {
                IEnumerable<DiscoveryResult> discovered = _discoverer.Discover().ToArray();

                // Updating unknown services
                foreach (var s in discovered.Where(x => x.ContractType == null))
                {
                    s.ContractType = _allServices
                        .SelectMany(x => x.GetType().GetInterfaces())
                        .FirstOrDefault(t => t.Name == s.DiscoveredType);

                    if (s.ContractType == null)
                    {
                        _services.Notifier.Warning(
                            T("Discovered unknown service: '<b>{0}</b>' at '<b>{1}</b>'</br>"
                              + "Full name: '<i>{2}</i>'</br>Description: '<i>{3}</i>'",
                            s.DiscoveredType, s.ServiceAddress, s.ServiceName, s.ServiceDescription));
                    }
                }

                foreach (DiscoveryResult s in discovered.Where(x => x.ContractType != null))
                {
                    _services.Notifier.Information(
                        T("Discovered known service: '<b>{0}</b>' at '<b>{1}</b>'</br>"
                          + "Full name: '<i>{2}</i>'</br>Description: '<i>{3}</i>'",
                        s.DiscoveredType, s.ServiceAddress, s.ServiceName, s.ServiceDescription));
                }
            }
            catch (Exception ex)
            {
                Logger.Error(T("Error during service discovery").Text, ex);
                _services.Notifier.Error(T("Error during service discovery: {0}", ex.Message));
            }

            return RedirectToAction("Index", "Admin", new { area = "Settings", groupInfoId = "Astoria" });
        }
    }
}