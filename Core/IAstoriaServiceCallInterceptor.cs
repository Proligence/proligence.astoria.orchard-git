using Proligence.Astoria.Client;

namespace Proligence.Astoria.Orchard.Core
{
    /// <summary>
    /// Interface for implementing interceptors for a given IAstoriaService.
    /// </summary>
    /// <typeparam name="TService"></typeparam>
    public interface IAstoriaServiceCallInterceptor<out TService> : IAstoriaInterceptor
        where TService : class, IAstoriaService
    {
    }
}