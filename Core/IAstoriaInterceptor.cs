using Castle.DynamicProxy;

namespace Proligence.Astoria.Orchard.Core
{
    public interface IAstoriaInterceptor : IInterceptor
    {
        /// <summary>
        /// Notifies the interceptor singleton object.
        /// </summary>
        /// <param name="reason">Notify reason.</param>
        /// <param name="optionalData">Additional data</param>
        void Notify(string reason = null, object optionalData = null);
    }
}