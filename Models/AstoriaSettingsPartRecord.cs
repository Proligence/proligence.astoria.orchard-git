using Orchard.ContentManagement.Records;
using Orchard.Data.Conventions;

namespace Proligence.Astoria.Orchard.Models
{
    public class AstoriaSettingsPartRecord : ContentPartRecord 
    {
        [StringLengthMax]
        public virtual string ServiceSettings { get; set; }
    }
}