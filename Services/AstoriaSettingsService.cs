﻿using System;
using System.Collections.Generic;
using System.Linq;
using Orchard;
using Orchard.ContentManagement;
using Proligence.Astoria.Client;
using Proligence.Astoria.Orchard.Core;
using Proligence.Astoria.Orchard.Settings;

namespace Proligence.Astoria.Orchard.Services
{
    public class AstoriaSettingsService : IAstoriaSettingsService
    {
        private readonly IOrchardServices _services;

        public IEnumerable<IAstoriaService> Services
        {
            get
            {
                return _services.WorkContext.Resolve<IEnumerable<IAstoriaService>>();
            }
        }

        public AstoriaSettingsService(IOrchardServices services)
        {
            _services = services;
        }

        public IServiceSettings For<TService>() where TService : IAstoriaService
        {
            return For(typeof(TService));
        }

        public IServiceSettings For(Type type)
        {
            if (!typeof(IAstoriaService).IsAssignableFrom(type))
            {
                return null;
            }

            return _services.WorkContext
                .CurrentSite.As<IAstoriaSettings>()
                .ServiceSettings.FirstOrDefault(s => s.TypeName == type.Name);
        }

        public IEnumerable<IServiceSettings> All()
        {
            return _services.WorkContext.CurrentSite.As<IAstoriaSettings>().ServiceSettings;
        }

        public IEnumerable<IRuntimeSettings> AllRuntime()
        {
            return _services.WorkContext.CurrentSite.As<IAstoriaSettings>().RuntimeSettings;
        }

        public IEnumerable<ServiceInfo> GetGlobalInfo()
        {
            return Services.Select(a => a.GetServiceInfo());
        }
    }
}