﻿using Autofac;
using Proligence.Astoria.Client;
using Proligence.Astoria.Client.Discovery;

namespace Proligence.Astoria.Orchard.Core
{
    /// <summary>
    /// Configures dependency injection container for Astoria client library.
    /// </summary>
    public class AstoriaModule : Module
    {
        /// <summary>
        /// Override to add registrations to the container.
        /// </summary>
        /// <param name="builder">The builder through which components can be registered.</param>
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<AstoriaChannelFactory>()
                .As<IAstoriaChannelFactory>()
                .InstancePerMatchingLifetimeScope("shell");

            builder.RegisterGeneric(typeof(AstoriaServiceCallInterceptor<>)).As(
                typeof(IAstoriaServiceCallInterceptor<>)).InstancePerMatchingLifetimeScope("shell");

            builder.RegisterGeneric(typeof(ServiceChannelCache<>))
                .As(typeof(IServiceChannelCache<>)).InstancePerMatchingLifetimeScope("shell");

            builder.RegisterSource(new ServiceProxyRegistrationSource());

            builder.RegisterType<ServiceDiscoverer>().As<IServiceDiscoverer>();
        }
    }
}