namespace Proligence.Astoria.Orchard.Settings
{
    public interface IServiceSettings
    {
        string TypeName { get; set; }
        string Url { get; set; }
    }
}