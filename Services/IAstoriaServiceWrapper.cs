﻿namespace Proligence.Astoria.Orchard.Services
{
    using Proligence.Astoria.Client;

    /// <summary>
    /// Generic interface for implementing Astoria service wrappers.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IAstoriaServiceWrapper<T> where T : IAstoriaService
    {
        /// <summary>
        /// Gets the underlying Astoria service.
        /// </summary>
        T Service { get; }
    }
}