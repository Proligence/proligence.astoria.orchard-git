﻿using System.Linq;
using Castle.DynamicProxy;
using Orchard.Environment;
using Orchard.Environment.Extensions;
using Proligence.Astoria.Client;
using Proligence.Astoria.Orchard.Core;
using Proligence.Profiler.Services;
using StackExchange.Profiling;

namespace Proligence.Astoria.Orchard.Profiling
{
    [OrchardFeature("Proligence.Astoria.Profiling")]
    public class ProfiledAstoriaServiceCallInterceptor<TService> : IAstoriaServiceCallInterceptor<TService>
        where TService : class, IAstoriaService
    {
        private readonly Work<IProfilerService> _service;
        private readonly IServiceChannelCache<TService> _serviceChannelCache;

        public ProfiledAstoriaServiceCallInterceptor(
            Work<IProfilerService> service,
            IServiceChannelCache<TService> serviceChannelCache)
        {
            _service = service;
            _serviceChannelCache = serviceChannelCache;
        }

        public void Intercept(IInvocation i)
        {
            if (_service.Value == null)
            {
                i.Proceed();
                return;
            }

            var methodCallString = 
                "Astoria (" 
                + typeof(TService).Name 
                + "): " 
                + i.GetConcreteMethodInvocationTarget().Name 
                + "(" 
                + string.Join(", ", i.Arguments.Select(
                    a => a == null ? "null" : a.GetType().FullName == a.ToString() ? "[obj]" : a.ToString())) 
                + "). Url: "
                + (_serviceChannelCache.ServiceAddress ?? "<Refreshing>");

                using (_service.Value.Profiler.Step(methodCallString, ProfileLevel.Verbose))
                {
                    i.Proceed();
                }
        }

        /// <summary>
        /// Notifies the interceptor singleton object.
        /// </summary>
        /// <param name="reason">Notify reason.</param>
        /// <param name="optionalData">Additional data</param>
        public void Notify(string reason = null, object optionalData = null)
        {
        }
    }
}