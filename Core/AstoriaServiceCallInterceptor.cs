﻿using System;
using System.Linq;
using System.ServiceModel;
using Castle.DynamicProxy;
using Orchard.Environment;
using Orchard.Localization;
using Proligence.Astoria.Client;
using Proligence.Astoria.Orchard.Services;
using Proligence.Astoria.Orchard.Settings;
using Proligence.FluentQuery.Client.Contract;
using Proligence.FluentQuery.Client.Wcf;

namespace Proligence.Astoria.Orchard.Core
{
    public class AstoriaServiceCallInterceptor<TService> : IAstoriaServiceCallInterceptor<TService>
        where TService : class, IAstoriaService
    {
        private readonly IServiceChannelCache<TService> _serviceChannelCache;
        private readonly Work<IAstoriaSettingsService> _settings;

        public AstoriaServiceCallInterceptor(
            IServiceChannelCache<TService> serviceChannelCache, 
            Work<IAstoriaSettingsService> settings)
        {
            _serviceChannelCache = serviceChannelCache;
            _settings = settings;
            
            T = NullLocalizer.Instance;

            // Setting up configuration options. Don't know if ctor is a good way to do that.
            if (typeof(TService).IsDefined(typeof(UsesFluentQueryAttribute), true)) 
            {
                _serviceChannelCache
                    .ClearConfig()
                    .WithConfig(factory => FluentQueryWcfHelper.SetupFluentQueryForEndpoint(
                        ((ChannelFactory<TService>)factory).Endpoint));
            }
        }

        public Localizer T { get; set; }

        public void Intercept(IInvocation invocation)
        {
            if (string.IsNullOrWhiteSpace(_serviceChannelCache.ServiceAddress))
            {
                Type serviceInterface = invocation.Method.DeclaringType;
                if (invocation.Method.DeclaringType == typeof(IAstoriaService))
                {
                    // NOTE:
                    // To support service methods implemented in Astoria core (which are declared in IAstoriaService
                    // interface), we need to find the real service interface to get the proper settings. Otherwise
                    // we'll get service settings for IAstoriaService instead of the service interface!
                    
                    serviceInterface = invocation.Proxy.GetType().GetInterfaces().FirstOrDefault(
                        i => i != typeof(IAstoriaService) && typeof(IAstoriaService).IsAssignableFrom(i));
                }

                IServiceSettings settings = _settings.Value.For(serviceInterface);

                if (settings == null 
                    || string.IsNullOrWhiteSpace(settings.Url) 
                    || !Uri.IsWellFormedUriString(settings.Url, UriKind.Absolute))
                {
                    LocalizedString message = T(
                        "Cannot invoke '{0}' on '{1}'. Service URL has not been set or is incorrect.",
                        invocation.Method.Name,
                        invocation.TargetType.Name);

                    throw new InvalidOperationException(message.Text);
                }

                _serviceChannelCache.ServiceAddress = settings.Url;   
            }

            if (invocation.Method.ReturnType == typeof(void))
            {
                _serviceChannelCache.Use(service => { invocation.Method.Invoke(service, invocation.Arguments); });
            }
            else 
            {
                invocation.ReturnValue = _serviceChannelCache.Use(
                    service => invocation.Method.Invoke(service, invocation.Arguments));
            }
        }

        /// <summary>
        /// Notifies the interceptor singleton object.
        /// </summary>
        /// <param name="reason">Notify reason.</param>
        /// <param name="optionalData">Additional data</param>
        public void Notify(string reason = null, object optionalData = null)
        {
            // Clears the current service URL, what forces it to be reloaded with the next call.
            _serviceChannelCache.ServiceAddress = null;
        }
    }
}