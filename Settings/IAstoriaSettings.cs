using System.Collections.Generic;
using Orchard.ContentManagement;

namespace Proligence.Astoria.Orchard.Settings
{
    public interface IAstoriaSettings : IContent
    {
        IList<RuntimeSettings> RuntimeSettings { get; }
        IList<ServiceSettings> ServiceSettings { get; } 
    }
}