using System;
using System.Collections.Generic;
using System.Reflection;
using Proligence.Astoria.Client;

namespace Proligence.Astoria.Orchard.Settings
{
    public class RuntimeSettings : IRuntimeSettings
    {
        public Type Type { get; set; }
        public string TypeName { get; set; }
        public string Description { get; set; }
        public IEnumerable<MethodInfo> Operations { get; set; }
        public IAstoriaService Instance { get; set; }
    }
}