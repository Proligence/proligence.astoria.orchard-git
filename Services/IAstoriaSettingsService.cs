using System;
using System.Collections.Generic;
using Orchard;
using Proligence.Astoria.Client;
using Proligence.Astoria.Orchard.Settings;

namespace Proligence.Astoria.Orchard.Services
{
    public interface IAstoriaSettingsService : ISingletonDependency
    {
        IServiceSettings For<TService>() where TService : IAstoriaService;
        IServiceSettings For(Type type);
        IEnumerable<IServiceSettings> All();
        IEnumerable<IRuntimeSettings> AllRuntime();
        IEnumerable<ServiceInfo> GetGlobalInfo();
    }
}