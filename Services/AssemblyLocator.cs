﻿namespace Proligence.Astoria.Orchard.Services
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Web;
    using System.Web.Compilation;

    public static class AssemblyLocator 
    { 
        private static readonly ReadOnlyCollection<Assembly> AllAssemblies; 
        private static readonly ReadOnlyCollection<Assembly> DependencyFolderAssemblies; 
 
        static AssemblyLocator() 
        { 
            AllAssemblies = new ReadOnlyCollection<Assembly>( 
                BuildManager.GetReferencedAssemblies().Cast<Assembly>().ToList()); 
 
            var binFolderAssemblies = new List<Assembly>();

            string binFolder = HttpRuntime.AppDomainAppPath + "App_Data\\Dependencies";
            IList<string> dllFiles = Directory.GetFiles(binFolder, "*.dll", SearchOption.TopDirectoryOnly).ToList();

            foreach (string dllFile in dllFiles) 
            { 
                AssemblyName assemblyName = AssemblyName.GetAssemblyName(dllFile);
                Assembly locatedAssembly = Assembly.Load(assemblyName);
                binFolderAssemblies.Add(locatedAssembly); 
            }
 
            DependencyFolderAssemblies = new ReadOnlyCollection<Assembly> (binFolderAssemblies); 
        }
 
        public static IEnumerable<Assembly> GetAssemblies()
        { 
            return AllAssemblies; 
        }
 
        public static IEnumerable<Assembly> GetDependencyFolderAssemblies()
        {
            return DependencyFolderAssemblies; 
        }
    }
}