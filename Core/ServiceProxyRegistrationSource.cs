﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Autofac;
using Autofac.Builder;
using Autofac.Core;
using Castle.DynamicProxy;
using Proligence.Astoria.Client;
using Proligence.Astoria.Orchard.Services;
using Proligence.Helpers.Reflection;

namespace Proligence.Astoria.Orchard.Core
{
    public class ServiceProxyRegistrationSource : IRegistrationSource
    {
        /// <summary>
        /// Gets whether the registrations provided by this source are 1:1 adapters on top of other components 
        /// (I.e. like Meta, Func or Owned.)
        /// </summary>
        public bool IsAdapterForIndividualComponents { get { return false; } }
        private readonly ProxyGenerator _proxyBuilder = new ProxyGenerator();
        private readonly IEnumerable<Type> _interfaces;
        private readonly IDictionary<Type, IComponentRegistration> _registrations = new Dictionary<Type, IComponentRegistration>();
        private bool _isSet = false;

        public ServiceProxyRegistrationSource()
        {
            IEnumerable<Assembly> assemblies = 
                AppDomain.CurrentDomain.GetAssemblies()
                .Concat(AssemblyLocator.GetDependencyFolderAssemblies())
                .Distinct();

            _interfaces = assemblies.SelectMany(
                a => a.GetLoadableTypes().Where(
                    t => typeof(IAstoriaService).IsAssignableFrom(t)
                        && t.IsInterface && typeof(IAstoriaService) != t));
        }

        /// <summary>
        /// Retrieve registrations for an unregistered service, to be used by the container.
        /// </summary>
        /// <param name="service">The service that was requested.</param>
        /// <param name="registrationAccessor">A function that will return existing registrations for a service.</param>
        /// <returns>Registrations providing the service.</returns>
        public IEnumerable<IComponentRegistration> RegistrationsFor(
            Service service,
            Func<Service, IEnumerable<IComponentRegistration>> registrationAccessor)
        {
            var swt = service as IServiceWithType;
            if (swt == null || !typeof(IAstoriaService).IsAssignableFrom(swt.ServiceType))
            {
                return Enumerable.Empty<IComponentRegistration>();
            }

            Type def = swt.ServiceType;

            // Loading registrations
            if (!_isSet)
            {
                lock (_registrations)
                {
                    if (!_isSet)
                    {
                        foreach (Type iface in _interfaces.Where(typeof(IAstoriaService).IsAssignableFrom))
                        {
                            _registrations[iface] = BuildProxy(
                                iface,
                                new[] { typeof(IAstoriaService) },
                                new[] { iface, typeof(IAstoriaService) });
                        }

                        _isSet = true;
                    }
                }
            }

            if (def == typeof(IAstoriaService))
            {
                return _registrations.Select(kvp => kvp.Value);
            }

            IComponentRegistration registration;
            return _registrations.TryGetValue(def, out registration)
                       ? new[] { registration }
                       : Enumerable.Empty<IComponentRegistration>();
        }

        private IComponentRegistration BuildProxy(
            Type serviceType,
            IEnumerable<Type> otherInterfaces,
            IEnumerable<Type> registeredAs)
        {
            var interceptorType = typeof(IAstoriaServiceCallInterceptor<>).MakeGenericType(serviceType);
            var interceptorsType = typeof(IEnumerable<>).MakeGenericType(interceptorType);

            return RegistrationBuilder.ForDelegate(
                serviceType, (c, p) =>
                {
                    var interceptors = (IEnumerable<IInterceptor>)c.Resolve(interceptorsType);
                    var proxy = this._proxyBuilder.CreateInterfaceProxyWithoutTarget(
                        serviceType, otherInterfaces.ToArray(), new ProxyGenerationOptions(){}, interceptors.ToArray());
                    return proxy;
                }).As(registeredAs.ToArray())
                    .InstancePerMatchingLifetimeScope("shell")
                    .CreateRegistration();
        }
    }
}