using System;
using Orchard.Data.Migration;
using Orchard.Environment.Configuration;
using Orchard.Logging;

namespace Proligence.Astoria.Orchard
{
    public class Migrations : DataMigrationImpl
    {
        private readonly ShellSettings _shellSettings;

        public Migrations(ShellSettings shellSettings)
        {
            _shellSettings = shellSettings;
            Logger = NullLogger.Instance;
        }

        public ILogger Logger { get; set; }

        public int Create()
        {
            try {
                RenameTable(
                    "Proligence_Common_AstoriaSettingsPartRecord",
                    "Proligence_Astoria_Orchard_AstoriaSettingsPartRecord"
                );
            }
            catch(Exception ex)
            {
                Logger.Error(ex, "Cannot rename old AstoriaSettingsPartRecord table. Creating new, empty one instead.");
                SchemaBuilder.CreateTable("AstoriaSettingsPartRecord", table => table
                    .ContentPartRecord()
                    .Column<string>("ServiceSettings", c => c.Unlimited())
                );
            }

            return 1;
        }

        private void RenameTable(string oldName, string newName)
        {
            SchemaBuilder.ExecuteSql(string.Format("sp_rename '{0}', '{1}'", PrefixTableName(oldName), PrefixTableName(newName)),
                sql => sql
                    .ForProvider("SqlCe")
                    .ForProvider("SqlServer"));

            SchemaBuilder.ExecuteSql(string.Format("rename table `{0}` to `{1}`", PrefixTableName(oldName), PrefixTableName(newName)),
                sql => sql
                    .ForProvider("MySql"));
        }

        private string PrefixTableName(string tableName)
        {
            if (string.IsNullOrEmpty(_shellSettings.DataTablePrefix))
                return tableName;
            return _shellSettings.DataTablePrefix + "_" + tableName;
        }
    }
}