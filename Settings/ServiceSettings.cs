namespace Proligence.Astoria.Orchard.Settings
{
    public class ServiceSettings : IServiceSettings
    {
        public string TypeName { get; set; }
        public string Url { get; set; }
    }
}