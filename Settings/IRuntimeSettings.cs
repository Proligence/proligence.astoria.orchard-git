using System;
using System.Collections.Generic;
using System.Reflection;
using Proligence.Astoria.Client;

namespace Proligence.Astoria.Orchard.Settings
{
    public interface IRuntimeSettings
    {
        Type Type { get; set; }
        string TypeName { get; set; }
        string Description { get; set; }
        IEnumerable<MethodInfo> Operations { get; set; }
        IAstoriaService Instance { get; set; }
    }
}